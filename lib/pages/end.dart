import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'home.dart';

class end extends StatefulWidget {
  int score = 0;
  int max = 0;

  end({Key? key, required this.score, required this.max})
      : super(key: key);

  @override
  State<end> createState() => _endState();
}

class _endState extends State<end> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("End"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text("Your score is ${widget.score} out of ${widget.max}"),
            LinearProgressIndicator(
              value: widget.score / widget.max,
              backgroundColor: Colors.red,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Home()));
                },
                child: Text("Go back"))
          ],
        ),
      ),
    );
  }
}
