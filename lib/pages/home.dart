import 'dart:math';

import 'package:flutter/material.dart';

import 'end.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  int index = 0;
  int score = 0;
  Map<String, List<Map<String, bool>>> data = {
    "Was ist die Hauptstadt von Österreich?": [
      {"Wien": true},
      {"Prag": false},
      {"Rom": false},
    ],
    "Wie viele Einwohner hat Österreich?": [
      {"4 Mio": false},
      {"8,9 Mio": true},
      {"10 Mio": false},
      {"12 Mio": false},
    ],
    "Wie viele Einwohner hat Langenlois": [
      {"6.000": false},
      {"3.000": false},
      {"7.500": true},
    ],
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Quiz"),
        ),
        body: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          data.keys.elementAt(index),
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        ),
        ...data.values.elementAt(index).map((e) {
          return ElevatedButton(
              onPressed: () {
                if (e.values.elementAt(0)) {
                  setState(() {
                    score++;
                  });
                }
                if (index < data.length - 1) {
                  setState(() {
                    index++;
                  });
                } else {
                  Navigator.push(
                      context,
                  MaterialPageRoute(
                      builder: (context) => end(
                            score: score,
                            max: data.length,
                          )));
                }
              },
              child: Text(e.keys.elementAt(0)));
        }).toList()
    ]
        ));
  }
}